﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using WordParser.BusinessLibrary;
using WordParser.Domain;

namespace WordParser.Service
{
    public class WordParserService : IParserService
    {
        private IParser _parser;

        /// <summary>
        /// Initializes a new instance of the <see cref="WordParserService" /> class.
        /// </summary>
        public WordParserService(IParser parser)
        {
            _parser = parser;
        }

        /// <summary>
        /// Parses the specified file path.
        /// </summary>
        /// <param name="inputStream">The input stream.</param>
        /// <returns>
        /// The list of words and their unique count.
        /// </returns>
        public List<ParsedWord> Parse(Stream inputStream)
        {
            using (var sr = new StreamReader(inputStream))
            {
                String words = sr.ReadToEnd();
                return _parser.Parse(words);
            }
        }
    }
}