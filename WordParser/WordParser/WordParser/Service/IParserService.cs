﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordParser.Domain;

namespace WordParser.Service
{
    /// <summary>
    /// Interface to parser 
    /// </summary>
    public interface IParserService
    {
        /// <summary>
        /// Parses the specified file path.
        /// </summary>
        /// <param name="inputStream">The input stream.</param>
        /// <returns>
        /// The list of words and their unique count.
        /// </returns>
        List<ParsedWord> Parse(Stream inputStream);
    }
}
