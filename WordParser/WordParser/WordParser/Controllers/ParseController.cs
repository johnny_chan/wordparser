﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WordParser.Models;
using WordParser.Service;
using WordParser.Domain;

namespace WordParser.Controllers
{
    public class ParseController : Controller
    {
        private IParserService _parserService;

        public ParseController(IParserService parserService)
        {
            _parserService = parserService;
        }

        //
        // GET: /Parse/

        public ActionResult Index()
        {
            return View(new ParsedWordsViewModel()
                {
                    Words = new List<WordViewModel>()
                });
        }

        [HttpPost]
        public ActionResult Index(HttpPostedFileBase file)
        {
            var result = new ParsedWordsViewModel
            {
                Words = new List<WordViewModel>()
            };
 
            if (file != null && file.ContentLength > 0)
            {
                const string supportedType = "txt";
                var fileExt = System.IO.Path.GetExtension(file.FileName).Substring(1);

                if (fileExt != supportedType)
                {
                    ModelState.AddModelError("file", "File not supported. Only .txt files are valid.");
                    return View(result);
                }

                var parsedWords = _parserService.Parse(file.InputStream);

                foreach (var current in parsedWords)
                {
                    var converted = current.ToViewModel();
                    result.Words.Add(converted);
                }
                return View(result);
            }
            else
            {
                return View(result);
            }
        }

    }
}
