﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WordParser.Models
{
    public class WordViewModel
    {
        public string  Word { get; set; }
    
        public int Count { get; set; }
        
    }
}