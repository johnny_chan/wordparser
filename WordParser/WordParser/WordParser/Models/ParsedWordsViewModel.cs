﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WordParser.Models
{
    public class ParsedWordsViewModel
    {
        public List<WordViewModel> Words { get; set; }
    }
}