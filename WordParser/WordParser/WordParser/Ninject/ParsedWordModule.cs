﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject.Modules;
using WordParser.BusinessLibrary;
using WordParser.Domain;
using WordParser.Service;

namespace WordParser.Ninject
{
    /// <summary> 
    /// Ninject module to inject objects relating to parsed words.
    /// </summary>
    public class ParsedWordModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IParser>().To<BusinessLibrary.WordParser>();
            Bind<IParserService>().To<WordParserService>();
        }
    }
}