﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NUnit.Framework;

namespace WordParser.BusinessLibrary
{
    [TestFixture]
    public class WordParserTest
    {
        private WordParser _wordParser;

        [TestFixtureSetUp]
        public void SetUp()
        {
            _wordParser = new WordParser();    
        }

        [Test]
        public void Parse_WithEmptyString_ExpectEmptyList()
        {
            // arrange
            string content = string.Empty;

            // act
            var results = _wordParser.Parse(content);

            // assert
            const int expectedParsedWorkCount = 0;

            Assert.AreEqual(expectedParsedWorkCount, results.Count);
        }

        [Test]
        public void Parse_WithUniqueWords_ExpectPopulatedWordsCollection()
        {
            // arrange
            const string content = "Lorem ipsum dolor sit amet consectetur adipiscing elit Duis sagittis";

            // act
            var results = _wordParser.Parse(content);

            // assert
            const int expectedParsedWorkCount = 10;

            Assert.AreEqual(expectedParsedWorkCount, results.Count);
        }

        [Test]
        public void Parse_WithDuplicatedWords_ExpectedResultsToIncludeDuplication()
        {
            // arrange
            const string content = "Lorem  ipsum dolor sit amet consectetur Lorem ipsum adipiscing elit Duis sagittis";

            // act
            var results = _wordParser.Parse(content);

            // assert
            const int expectedParsedWorkCount = 10;
            const int expectedParsedWorkCountLoremWordCount = 2;
            const int expectedParsedWorkCountIpsumWordCount = 2;

            Assert.AreEqual(expectedParsedWorkCount, results.Count);
            Assert.AreEqual(expectedParsedWorkCountLoremWordCount, results[0].Count);
            Assert.AreEqual(expectedParsedWorkCountIpsumWordCount, results[1].Count);
        }

        [Test]
        public void Parse_WithDuplicatedWordsInUpperAndLowerCase_ExpectDuplicatedWordsToIncluded()
        {
            // arrange
            const string content = "Lorem lorem ipsum dolor sit amet consectetur Ipsum adipiscing elit Duis sagittis";

            // act
            var results = _wordParser.Parse(content);

            // assert
            const int expectedParsedWorkCount = 10;
            const int expectedParsedWorkCountLoremWordCount = 2;
            const int expectedParsedWorkCountIpsumWordCount = 2;

            Assert.AreEqual(expectedParsedWorkCount, results.Count);
            Assert.AreEqual(expectedParsedWorkCountLoremWordCount, results[0].Count);
            Assert.AreEqual(expectedParsedWorkCountIpsumWordCount, results[1].Count);
        }

        [Test]
        public void Parse_WithNonWordCharactersAndExtraSpaces_ExpectResultsToExcludeCharacters()
        {
            const string content = "Lorem lorem ipsum dolor? sit amet, consectetur !   ipsum adipiscing elit. Duis sagittis.";
            // act
            var results = _wordParser.Parse(content);

            // assert
            const int expectedParsedWorkCount = 10;
            const int expectedParsedWorkCountLoremWordCount = 2;
            const int expectedParsedWorkCountIpsumWordCount = 2;

            Assert.AreEqual(expectedParsedWorkCount, results.Count);
            Assert.AreEqual(expectedParsedWorkCountLoremWordCount, results[0].Count);
            Assert.AreEqual(expectedParsedWorkCountIpsumWordCount, results[1].Count);
        }
    }
}