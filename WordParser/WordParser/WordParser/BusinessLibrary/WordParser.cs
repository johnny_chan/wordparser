﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WordParser.Domain;

namespace WordParser.BusinessLibrary
{
    /// <summary>
    /// 
    /// </summary>
    public class WordParser : IParser
    {
        /// <summary>
        /// Parses the specified text
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns>
        /// Returns each unique word and its count.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public List<ParsedWord> Parse(string text)
        {
            var wordSeparators = new char[] {' ', ',', '.', '!', '?'};

            var words = text.Split(wordSeparators, StringSplitOptions.RemoveEmptyEntries);

            return process(words);
        }

        /// <summary>
        /// Process the words and count number of instances for each unique word.
        /// </summary>
        /// <param name="words">The words.</param>
        /// <returns></returns>
        private List<ParsedWord> process(string[] words)
        {
            var filterWords = new List<ParsedWord>();

            foreach (var currentWord in words)
            {
                if (filterWords.Any(w => w.Word.ToLower() == currentWord.ToLower()))
                {
                    var parsedWord = filterWords.First(w => w.Word == currentWord.ToLower());
                    parsedWord.Count += 1;
                }
                else
                {
                    filterWords.Add(new ParsedWord
                        {
                            Word = currentWord.ToLower(),
                            Count = 1
                        });
                }
            }

            return filterWords;
        } 
    }
}