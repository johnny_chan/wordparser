﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordParser.Domain;

namespace WordParser.BusinessLibrary
{
    public interface IParser
    {
        /// <summary>
        /// Parses the specified text
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns>Returns each unique word and its count.</returns>
        List<ParsedWord> Parse(string text);
    }
}
