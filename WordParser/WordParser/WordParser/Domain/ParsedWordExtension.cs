﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WordParser.Models;

namespace WordParser.Domain
{
    public static class ParsedWordExtension
    {
        /// <summary>
        /// Converts domain model to the view model.
        /// </summary>
        /// <param name="parsedWord">The parsed word.</param>
        /// <returns></returns>
        public static WordViewModel ToViewModel(this ParsedWord parsedWord)
        {
            return new WordViewModel()
                {
                    Word = parsedWord.Word,
                    Count = parsedWord.Count
                };
        }
    }
}