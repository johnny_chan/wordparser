﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WordParser.Domain
{
    /// <summary>
    /// Encapsulates word and its
    /// </summary>
    public class ParsedWord
    {
        public string Word { get; set; }

        public int Count { get; set; }        
    }
}